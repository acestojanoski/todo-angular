import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Todo } from '../models/todo.model';

@Injectable()
export class TodoService {
  constructor(private http: HttpClient) {

  }

  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }

  getTodos(): Observable<any> {
    return this.http.get(environment.apiUrl).pipe(
      catchError(this.handleError)
    );
  }

  createTodo(todo: Todo): Observable<any> {
    return this.http.post<any>(environment.apiUrl, todo).pipe(
      catchError(this.handleError)
    )
  }

  deleteTodo(id: string): Observable<any> {
    return this.http.delete(`${environment.apiUrl}/${id}`).pipe(
      catchError(this.handleError)
    )
  }
}
