import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TodoService } from 'src/app/services/todo.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-insert-todo',
  templateUrl: './insert-todo.component.html',
  styleUrls: ['insert-todo.component.scss']
})

export class InsertTodoComponent implements OnInit {
  todoInput = '';
  @Output() insertedTodo: EventEmitter<any> = new EventEmitter();

  constructor(public rest: TodoService, private toastr: ToastrService) {

  }

  ngOnInit() {

  }

  onInsert() {
    let data = {
      description: this.todoInput,
    }

    this.rest.createTodo(data).subscribe(response => {
      this.toastr.success('Todo inserted', '');
      this.insertedTodo.emit();
    }, (err) => {
      console.log(err);
      this.toastr.error(err.error.error, '');
    });

    this.todoInput = '';
  }
}
