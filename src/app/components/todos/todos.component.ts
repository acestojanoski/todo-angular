import { Component, OnInit } from '@angular/core';
import { TodoService } from 'src/app/services/todo.service';
import { ToastrService, Toast } from 'ngx-toastr';

@Component({
    selector: 'app-todos',
    templateUrl: './todos.component.html',
    styleUrls: [
      './todos.component.scss',
    ]
})

export class TodosComponent implements OnInit {
    todos = [];
    errorGettingTodos = false;

    constructor(public rest: TodoService, private toastr: ToastrService) {

    }

    ngOnInit() {
        this.getTodos();
    }

    onInsertedTodo() {
        this.getTodos();
    }

    getTodos() {
        this.todos = null;
        this.rest.getTodos().subscribe(response => {
            this.todos = response;
        }, (err) => {
            console.log(err);
            this.toastr.error('Failed to load todos', '');
        });

        this.todos = [];
    }

    removeTodo(id) {
        this.rest.deleteTodo(id).subscribe(response => {
            this.toastr.success('Todo removed', '');
            this.getTodos();
        }, (err) => {
          this.toastr.error(err.error.error, '');
        });
    }
}
