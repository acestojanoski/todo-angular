import { Component } from '@angular/core';

@Component({
  selector: 'app-header',
  template: `
  <nav>
    <a routerLink="/" routerLinkActive="active">Home</a>
    <a routerLink="/about" routerLinkActive="active">About</a>
  </nav>
  `,
  styles: [
    'nav { padding: 20px 20px 20px 0; }',
    'nav a { padding: 10px; text-decoration: none; background: #fff; border-radius: 3px; color: rgb(0, 110, 255); font-weight: bold; margin-right: 15px;}'
  ]
})

export class HeaderComponent {
  constructor() {

  }
}
